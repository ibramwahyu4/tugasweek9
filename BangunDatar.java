package week9;

public class BangunDatar {

    private int panjang;
    private int lebar;
    // private int diameter;
    // private double radius;
    protected int sisi;
    protected static final double pi = 3.14;

    public BangunDatar(int sisi)
    {
        this.sisi = sisi;
    }

    public BangunDatar(int panjang, int lebar)
    {
        this.panjang = panjang;
        this.lebar = lebar;
    }

    //Mencari Luas

    public int luas(int a)
    {
        return a*a;
    }

    public int luas(int a, int b)
    {
        return a*b;
    }

    public double luas(double a, double b)
    {
        return (a*b)/2;
    }

    public double luas(double a)
    {
        return(double)pi*a*a;
    }

    //Mencari Keliling
    public int keliling(int a)
    {
        return 4*a;
    }

    public int Keliling(int a, int b)
    {
        return (2*a)+(2*b);
    }

    public double Keliling(double a)
    {
        return (double)pi*a;
    }

    //Getter
    public int getSisi()
    {
        return sisi;
    }

    public int getPanjang()
    {
        return panjang;
    }

    public int getLebar()
    {
        return lebar;
    }
}