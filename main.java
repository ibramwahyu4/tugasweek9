package week9;

public class main 
{
    public static void main(String[] args)
    {
        //buat luas & keliling 
        BangunDatar persegi   = new BangunDatar(2);
        BangunDatar pp        = new BangunDatar(2,3);
        BangunDatar segitiga  = new BangunDatar(4,2);
        BangunDatar lingkaran = new BangunDatar(6);

        //Buat Volume
        BangunRuang balok = new BangunRuang(5, 4, 5);
        BangunRuang kubus = new BangunRuang(7);

        //Hitung Luas
        System.out.println("Hasil Luas Persegi = "+persegi.luas(2));
        System.out.println("Hasil Luas Persegi Panjang = "+pp.luas(2, 3));
        System.out.println("Hasil Luas Segitiga = "+segitiga.luas(4,2));
        System.out.println("hasil Luas Lingkaran = "+lingkaran.luas(6));
        System.out.println("  ");

        //Hitung Keliling
        System.out.println("Hasil Keliling persegi = "+persegi.keliling(4));
        System.out.println("hasil Keliling Peresegi Panjang = "+pp.Keliling(5, 3));
        System.out.println("Hasil Keliling Lingkaran = "+lingkaran.Keliling(7));
        System.out.println("   ");

        //Hitung Volume
        System.out.println("Hasil Volume Balok = "+balok.volume(5, 4, 5));
        System.out.println("Hasil Volume Kubus = "+kubus.volume(4));

    }    
}
